//
//  Sales.swift
//  HelloTreeline
//
//  Created by Fabian E. Pezet Vila on 28/08/2021.
//

import Foundation

struct Sale {
    let itemId: String
    let date: Date
    var quantity = 0
}
