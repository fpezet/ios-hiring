//
//  Date+Additions.swift
//  HelloTreeline
//
//  Created by Fabian E. Pezet Vila on 28/08/2021.
//

import Foundation

extension Date {

    /// Date without time
    var onlyDate: Date? {
            let calender = Calendar.current
            var dateComponents = calender.dateComponents([.year, .month, .day], from: self)
            dateComponents.timeZone = NSTimeZone.system
            return calender.date(from: dateComponents)
    }
    
    var shortFormatt: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd,yyyy"
        return formatter.string(from: self)
    }
    
}
