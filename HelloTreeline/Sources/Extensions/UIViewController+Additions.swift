//
//  UIViewController+Additions.swift
//  HelloTreeline
//
//  Created by Fabian E. Pezet Vila on 30/08/2021.
//

import UIKit
import MBProgressHUD

protocol Loadble {
    func showIndicator(title: String?, description: String?)
    func hideIndicator()
}

extension UIViewController {
   func showIndicator(title: String? = nil, description: String? = nil) {
        let Indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        Indicator.isUserInteractionEnabled = false
        Indicator.label.text = title ?? ""
        Indicator.detailsLabel.text = description ?? ""
        Indicator.show(animated: true)
   }
   func hideIndicator() {
      MBProgressHUD.hide(for: self.view, animated: true)
   }
}
