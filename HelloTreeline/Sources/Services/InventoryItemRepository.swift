//
//  InventoryItemRepository.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

protocol InventoryItemRepository {
    func getAll(completion: @escaping (Result<[InventoryItem], Error>) -> Void)
    func get(objectId: String) -> InventoryItem?
    func getSales() -> [Sale]
    func save(sale: Sale)
    func save(item: InventoryItem)
}

class DefaultInventoryItemRepository: InventoryItemRepository {
    
    static let shared: InventoryItemRepository = DefaultInventoryItemRepository()
    
    private let apiClient: APIClient
    
    private var items: [InventoryItem] = []
    
    private var sales: [Sale] = []
    
    init(apiClient: APIClient = DefaultAPIClient.shared) {
        self.apiClient = apiClient
    }
    
    func getAll(completion: @escaping (Result<[InventoryItem], Error>) -> Void) {
        apiClient.get(endpoint: "getInventory", type: [InventoryItem].self) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let items):
                    self.items = items
                    completion(.success(self.items))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    func get(objectId: String) -> InventoryItem? {
        items.first(where: { $0.id == objectId })
    }
    
    func getSales() -> [Sale] {
        return sales
    }
    
    func save(sale: Sale) {
        guard let index = sales.firstIndex(where: { $0.itemId == sale.itemId && $0.date == sale.date }) else {
            sales.append(sale)
            return
        }
        sales[index] = sale
    }
    
    func save(item: InventoryItem) {
        guard let index = items.firstIndex(where: { $0.id == item.id }) else { return }
        items[index] = item
    }
}
