//
//  DetailsInteractor.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

protocol DetailsInteractorDelegate: AnyObject {
    func itemDidLoad(item: InventoryItem)
    func saleDidFound(sale: Sale?)
    func saleDidSaved(sale: Sale)
    func itemDidSave(item: InventoryItem)
}

class DetailsInteractor {

    weak var delegate: DetailsInteractorDelegate?
    
    private let itemRepository: InventoryItemRepository

    init(itemRepository: InventoryItemRepository = DefaultInventoryItemRepository.shared) {
        self.itemRepository = itemRepository
    }
    
    func loadItem(objectId: String) {
        guard let item = itemRepository.get(objectId: objectId) else { return }
        delegate?.itemDidLoad(item: item)
    }
    
    func save(item: InventoryItem) {
        itemRepository.save(item: item)
        delegate?.itemDidSave(item: item)
    }
    
    func fetch(sale objectId: String, date: Date) {
        let sales = itemRepository.getSales()
        let sale = sales.first(where: { $0.itemId == objectId && $0.date == date })
        delegate?.saleDidFound(sale: sale)
    }
    
    func save(sale: Sale) {
        itemRepository.save(sale: sale)
        delegate?.saleDidSaved(sale: sale)
    }
    
    
}
