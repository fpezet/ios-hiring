//
//  DetailsPresenter.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

class DetailsPresenter: DetailsInteractorDelegate {
    
    private let router: DetailsRouter
    private let interactor: DetailsInteractor
    private weak var view: DetailsViewController!
    
    private let objectId: String
    private var item: InventoryItem?
    private var sale: Sale?
    
    init(view: DetailsViewController,
         interactor: DetailsInteractor,
         router: DetailsRouter,
         objectId: String) {
        
        self.view = view
        self.interactor = interactor
        self.router = router

        self.objectId = objectId
        
        self.interactor.delegate = self
    }
    
    func viewDidLoad() {
        view.isAddOneItemButtonsEnabled = false
        view.isRemoveOneItemButtonsEnabled = false
        interactor.loadItem(objectId: objectId)
    }

    func incrementSale() {
        guard let item = item, let sale = sale, item.available - sale.quantity > 0 else { return }
        var updatedSale = sale
        updatedSale.quantity += 1
        interactor.save(sale: updatedSale)
    }
    
    func decrementSale() {
        guard let sale = sale, sale.quantity > 0 else { return }
        var updatedSale = sale
        updatedSale.quantity -= 1
        interactor.save(sale: updatedSale)
    }
    
    //MARK: - DetailsInteractorDelegate
    func itemDidLoad(item: InventoryItem) {
        self.item = item
        view.navBarTitle = item.title
        view.idLabelTitle = "ID: \(item.id)"
        view.titleLabelTitle = "Title: \(item.title)"
        view.desciptionLabelTitle = "Description: \(item.description)"
        view.colorLabelTitle = "Color: \(item.color)"
        view.costLabelTitle = "Cost: \(String(describing: item.cost))"
        view.availableLabelTitle = "Available: \(item.available)"
        view.soldLabelTitle = ""
        
        guard let dayKey = Date().onlyDate else {
            router.show(alert: "Error", message: "Unable to determine current date")
            return
        }
        interactor.fetch(sale: objectId, date: dayKey)
    }
    
    func saleDidFound(sale: Sale?) {
        if let savedSale = sale {
            self.sale = savedSale
            guard let item = item else { return }
            updateQuantities(available: item.available - savedSale.quantity,
                             sold: savedSale.quantity)
            updateAtions()
        } else {
            guard let dayKey = Date().onlyDate else {
                router.show(alert: "Error", message: "Unable to determine current date")
                return
            }
            let sale = Sale(itemId: objectId, date: dayKey)
            interactor.save(sale: sale)
        }
    }

    func saleDidSaved(sale: Sale) {
        self.sale = sale
        guard var item = item else { return }
        item.available -= sale.quantity
        interactor.save(item: item)
    }
    
    func itemDidSave(item: InventoryItem) {
        guard let sale = sale else { return }
        updateQuantities(available: item.available,
                         sold: sale.quantity)
        updateAtions()
    }

    // MARK: - Private
    private func updateQuantities(available: Int, sold: Int) {
        view.availableLabelTitle = "Available: \(available)"
        view.soldLabelTitle = "Sold: \(sold)"
    }
    
    private func updateAtions() {
        guard let item = item else { return }
        if item.available > 0 {
            view.isAddOneItemButtonsEnabled = true
            view.isRemoveOneItemButtonsEnabled = true
        }
    }
}


