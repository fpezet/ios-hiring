//
//  DetailsRouter.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
import UIKit

protocol DetailsRouter: AnyObject {
    func show(alert: String, message: String)
}

class DetailsDefaultRouter: DetailsRouter {

    private weak var viewController: DetailsDefaultViewController!

    public init(viewController: DetailsDefaultViewController) {
        self.viewController = viewController
    }
    
    func show(alert: String, message: String) {
        let alert = UIAlertController(title: alert, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        viewController.present(alert, animated: true)
    }
}
