//
//  ListRouter.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

protocol ListRouter: AnyObject {
    func routeToDetails(objectId: String)
    func show(alert: String, message: String)
    func routeToMailComposer(with file: URL)
    func dismiss(animated: Bool)
}

class ListDefaultRouter: ListRouter {

    private weak var viewController: ListDefaultViewController!

    public init(viewController: ListDefaultViewController) {
        self.viewController = viewController
    }

    func routeToDetails(objectId: String) {
        let detailsViewController = DetailsDefaultViewController.build(objectId: objectId)
        viewController.navigationController?.pushViewController(detailsViewController, animated: true)
    }
    
    func show(alert: String, message: String) {
        let alert = UIAlertController(title: alert, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        viewController.present(alert, animated: true)
    }
    
    func routeToMailComposer(with file: URL) {
        if( MFMailComposeViewController.canSendMail()){

            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = viewController
            mailComposer.setToRecipients(["bossman@bosscompany.com"])
            mailComposer.setSubject("SalesReport")

            mailComposer.setMessageBody("This is an example", isHTML: true)
            
            if let fileData = NSData(contentsOf: file) {
                mailComposer.addAttachmentData(fileData as Data, mimeType: "text/csv", fileName: "SalesReport.csv")
            }

            //this will compose and present mail to user
            viewController.present(mailComposer, animated: true, completion: nil)
            } else {
                show(alert: "Error", message: "email is not supported")
            }
    }
    
    func dismiss(animated: Bool) {
        viewController.dismiss(animated: animated, completion: nil)
    }
}
