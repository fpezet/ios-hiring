//
//  ReportBuilder.swift
//  HelloTreeline
//
//  Created by Fabian E. Pezet Vila on 30/08/2021.
//

import Foundation

protocol ReportBuilderDelegate: AnyObject {
    func didFinishBuildReport(uri: URL)
    func didFinishWithError(message: String)
}

class ReportBuilder {
    
    enum Report {
        case sales
        case other
    }
    
    private weak var delegate: ReportBuilderDelegate?
    private let itemRepository: InventoryItemRepository
    
    var report: Report = .other
    
    init(delegate: ReportBuilderDelegate,
         itemRepository: InventoryItemRepository = DefaultInventoryItemRepository.shared) {
        self.delegate = delegate
        self.itemRepository = itemRepository
    }
    
    func build() {
        switch report {
        case .sales:
            buildSalesReport()
        default:
            break
        }
        
    }
}

extension ReportBuilder {
    private func buildSalesReport() {
        let sales = itemRepository.getSales()
        let itemIds = Set(sales.map { $0.itemId })
        
        var csvString = "Item Id; Date; Quantity\n"
        for itemId in itemIds {
            let salesByDay = sales.filter { $0.itemId == itemId }.sorted(by: { $0.date < $1.date})
            let rawSalesByDay = salesByDay.compactMap { "\($0.itemId); \($0.date.shortFormatt); \($0.quantity)\n" }
            csvString.append(rawSalesByDay.joined())
        }
        
        let fileManager = FileManager.default
        do {
            let path = try fileManager.url(for: .documentDirectory,
                                           in: .allDomainsMask,
                                           appropriateFor: nil,
                                           create: false)
            let fileURL = path.appendingPathComponent("SalesReport.csv")
            try csvString.write(to: fileURL, atomically: true, encoding: .utf8)
            
            delegate?.didFinishBuildReport(uri: fileURL)
        } catch {
            delegate?.didFinishWithError(message: "Fail to create report")
        }
        
        
    }
}
