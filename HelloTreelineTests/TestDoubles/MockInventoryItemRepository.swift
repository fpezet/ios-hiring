//
//  MockInventoryItemRepository.swift
//  HelloTreelineTests
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
@testable import HelloTreeline

class MockInventoryItemRepository: InventoryItemRepository {
    
    var items: [InventoryItem] = []
    var sales: [Sale] = []
    
    func getAll(completion: @escaping (Result<[InventoryItem], Error>) -> Void) {
        completion(.success(items))
    }
    
    func get(objectId: String) -> InventoryItem? {
        items.first(where: { $0.id == objectId })
    }
    
    func getSales() -> [Sale] {
        return sales
    }
    
    func save(sale: Sale) {
        sales.append(sale)
    }
    
    func save(item: InventoryItem) {
        
    }
}
