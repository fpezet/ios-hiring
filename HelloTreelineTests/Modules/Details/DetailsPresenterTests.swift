//
//  DetailsPresenterTests.swift
//  HelloTreelineTests
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import XCTest
@testable import HelloTreeline

class DetailsPresenterTests: XCTestCase {
    
    private var presenter: DetailsPresenter!
    private var view: MockViewController!
    
    private let inventoryItem  = InventoryItem(id: "test-id", title: "title", description: "description", color: "color", available: 100, cost: 9.99)
    private let sale = Sale(itemId: "test-id", date: Date().onlyDate!)

    override func setUpWithError() throws {
        
        self.view = MockViewController()
        
        let itemRepository = MockInventoryItemRepository()
        itemRepository.items = [inventoryItem]
        itemRepository.sales = [sale]
        let interactor = DetailsInteractor(itemRepository: itemRepository)
        self.presenter = DetailsPresenter(view: view, interactor: interactor, router: StubRouter(), objectId: inventoryItem.id)
    }

    func testViewDidLoadToPopulateAllLabels() throws {
        
        // before
        XCTAssertNil(view.navBarTitle)
        XCTAssertNil(view.idLabelTitle)
        XCTAssertNil(view.titleLabelTitle)
        XCTAssertNil(view.desciptionLabelTitle)
        XCTAssertNil(view.colorLabelTitle)
        XCTAssertNil(view.costLabelTitle)
        XCTAssertNil(view.availableLabelTitle)
        XCTAssertNil(view.soldLabelTitle)
        XCTAssertEqual(view.isAddOneItemButtonsEnabled, false)
        XCTAssertEqual(view.isRemoveOneItemButtonsEnabled, false)
        
        // when
        presenter.viewDidLoad()
        
        // then
        XCTAssertEqual(view.navBarTitle, "\(inventoryItem.title)")
        XCTAssertEqual(view.idLabelTitle, "ID: \(inventoryItem.id)")
        XCTAssertEqual(view.titleLabelTitle, "Title: \(inventoryItem.title)")
        XCTAssertEqual(view.desciptionLabelTitle, "Description: \(inventoryItem.description)")
        XCTAssertEqual(view.colorLabelTitle, "Color: \(inventoryItem.color)")
        XCTAssertEqual(view.costLabelTitle, "Cost: \(String(describing: inventoryItem.cost))")
        XCTAssertEqual(view.availableLabelTitle, "Available: \(String(describing: inventoryItem.available))")
        XCTAssertEqual(view.soldLabelTitle, "Sold: 0")
        XCTAssertEqual(view.isAddOneItemButtonsEnabled, true)
        XCTAssertEqual(view.isRemoveOneItemButtonsEnabled, true)
    }
    
    
    func testIncrementDecrementSale() throws {

        presenter.viewDidLoad()
        XCTAssertEqual(view.availableLabelTitle, "Available: \(String(describing: inventoryItem.available))")
        XCTAssertEqual(view.soldLabelTitle, "Sold: 0")

        presenter.incrementSale()
        
        XCTAssertEqual(view.availableLabelTitle, "Available: \(String(describing: inventoryItem.available - 1))")
        XCTAssertEqual(view.soldLabelTitle, "Sold: 1")
        
        presenter.decrementSale()
        
        XCTAssertEqual(view.availableLabelTitle, "Available: \(String(describing: inventoryItem.available))")
        XCTAssertEqual(view.soldLabelTitle, "Sold: 0")

    }
    
}

fileprivate class MockViewController: DetailsViewController {
    
    var navBarTitle: String?
    
    var idLabelTitle: String?
    
    var titleLabelTitle: String?
    
    var desciptionLabelTitle: String?
    
    var colorLabelTitle: String?
    
    var costLabelTitle: String?

    var availableLabelTitle: String?
    
    var soldLabelTitle: String?
    
    var isAddOneItemButtonsEnabled: Bool = false
    
    var isRemoveOneItemButtonsEnabled: Bool = false

}

fileprivate class StubRouter: DetailsRouter {
    func show(alert: String, message: String) {
        
    }
}
